import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ForecastService } from './forecast.service';
import { EMPTY } from 'rxjs';

interface GetCoordsAction extends Object{
  type: string,
  cityName: string
}

interface GetForecastAction extends Object{
  type: string,
  payload: {
    data: { lat: number, lon: number }[],
    cityName: string
  }
}

interface GetCitiesAction extends Object{
  type: string,
  cityName: string
}

@Injectable()
export class ForecastEffects {

  constructor(
    private actions$: Actions,
    private fs: ForecastService
  ) {}

  getCoords = createEffect(() => this.actions$.pipe(
      ofType('[FORECAST] GET COORDS'),
      mergeMap((action: GetCoordsAction) =>
        this.fs.getCoordinates(action.cityName)
        .pipe(
          map((coords) => ({
            type: '[FORECAST] GET',
            payload: {data: coords, cityName: action.cityName}
          })),
          catchError(() => EMPTY)
        )
      )
  ));

  getForecast = createEffect(() => this.actions$.pipe(
    ofType('[FORECAST] GET'),
    mergeMap((action: GetForecastAction) =>
    {
      const data = action.payload.data[0];
      return this.fs.getForecast(
        {
          lat: data.lat,
          lon: data.lon
        })
        .pipe(
          map((forecast: any) => {
            return {
              type: '[FORECAST] GET SUCCESS',
              payload: {
                data: {
                  daily: forecast.daily,
                  hourly: forecast.hourly
                },
                cityName: action.payload.cityName
              }
            }
          }),
          catchError(() => EMPTY)
        )
    })
  ));

  getCities = createEffect(() => this.actions$.pipe(
    ofType('[CITIES] GET'),
    mergeMap((action: GetCitiesAction) =>
      this.fs.getCities(action.cityName)
        .pipe(
          map((cities) => ({
            type: '[CITIES] GET SUCCESS',
            payload: cities })),
          catchError(() => EMPTY)
        )
    )
  ));

}
