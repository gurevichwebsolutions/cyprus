import { createAction, props } from '@ngrx/store';

export const getCoords = createAction(
  '[FORECAST] GET COORDS',
  props<{cityName: string}>()
);

export const getForecast = createAction(
  '[FORECAST] GET',
  props<any>()
);

export const getForecastSuccess = createAction(
  '[FORECAST] GET SUCCESS',
  props<any>()
);

export const getCities = createAction(
  '[CITIES] GET',
  props<any>()
);

export const getCitiesSuccess = createAction(
  '[CITIES] GET SUCCESS',
  props<any>()
);
