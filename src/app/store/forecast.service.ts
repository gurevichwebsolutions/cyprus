import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { map } from "rxjs/operators";



@Injectable()
export class ForecastService {
  private API_KEY = 'c03b0c6eb7ceedb91c8f1660cb45899f';
  private DADATA_KEY = 'f7f5ae0495a20b7a8b3c6209bba7d94aa8ba44e8';
  constructor(private http: HttpClient) { }

  getCoordinates(cityName: string){
    const URL = 'https://api.openweathermap.org/geo/1.0/direct';
    return this.http.get(
      `${URL}?q=${cityName}&limit=1&appid=${this.API_KEY}`)
  }

  getForecast(data: {lat: number, lon: number}){
    const URL = 'https://api.openweathermap.org/data/2.5/onecall';
    return this.http.get(
      `${URL}?lat=${data.lat}&lon=${data.lon}&units=metric&appid=${this.API_KEY}`);
  }

  getCities(name: string): Observable<any>{
    return this.http.post(
      'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
      {
        query: name,
        language: 'en',
        locations: [{'country': '*'}]
      },
      {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": `Token ${this.DADATA_KEY}`
        }
    })
      .pipe(
        map((ev: any) => ev.suggestions.filter((el: any) => el.data.city)));
  }
}


