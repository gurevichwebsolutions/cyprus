import {Action, ActionReducer, createReducer, on} from '@ngrx/store';
import * as forecastActions from './actions';


export class ForecastReducer {

  static forecast(): ActionReducer<any, Action> {
    return createReducer(
      {
        state: forecastActions.getForecastSuccess.type,
        payload: {
          data: [],
          cityName: ''
        }
      },
      on(
        forecastActions.getForecastSuccess,
        (state, action) => {
          return {
            state: action.type,
            payload: action.payload
          }
      })
    );
  }

  static cities(): ActionReducer<any, Action> {
    return createReducer(
      {
        state: forecastActions.getCitiesSuccess.type,
        payload: []
      },
      on(forecastActions.getCitiesSuccess,
        (state, action) => ({state: action.type, payload: action.payload}))
    );
  }
}
