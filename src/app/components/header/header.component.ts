import { Component, OnChanges, OnInit } from '@angular/core';
import { FormControl } from "@angular/forms";
import {async, Observable} from "rxjs";
import { map, startWith } from "rxjs/operators";
import { ForecastService } from "../../store/forecast.service";
import {Store} from "@ngrx/store";
import {getCities, getCoords} from "../../store/actions";

export interface User {
  name: string;
}

interface Cities extends Object{
  state: string,
  payload: {
    data: {
      city: string
    }
  }[]
}
//AIzaSyCMRsnc-09LGuGbYF4G8CBR8iXwz1Oqz8E

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  cityName = new FormControl();
  cities$?: Observable<Cities>;

  constructor(
    private fs: ForecastService,
    private store: Store<any>
  ) {
    this.cities$ = this.store.select('cities');
  }

  ngOnInit() {
  }

  print = (o: any) => console.log(o);

  getCities(): void{
    const cityName = this.cityName.value;
    this.store.dispatch(getCities({ cityName }));
  }

  getForecast(cityName: string): void{
    this.cityName.setValue(cityName);
    this.store.dispatch(getCoords({ cityName }));
  }
}
