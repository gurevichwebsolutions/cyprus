import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';

interface Payload extends Object{
  data: Data,
  cityName: string
}

interface Data extends Object{
  daily: any[],
  hourly: any[],
  cityName: string
}


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  forecast$?: Observable<any>;

  dailyCols: string[] =
  [
    'cityName',
    'mon',
    'tue',
    'wed',
    'thu',
    'fri',
    'sat',
    'sun'
  ];

  hourlyCols: string[] =
    [
      'cityName',
      'col_00',
      'col_03',
      'col_06',
      'col_09',
      'col_12',
      'col_15',
      'col_18',
      'col_21'
    ];

  constructor(
    private store: Store<any>) {
      this.forecast$ = this.store.select('forecast');
  }

  ngOnInit(): void {}

  dailyPipe(payload: Payload): any[]{
    let result: any = {}

    if(payload.data.daily){
      payload.data.daily
        .map((el: any, i: number) => {
          if(i < 7){
            const day = formatDate(
              el.dt*1000,
              'EE',
              'en-US')
              .toLowerCase();
            result[day] = el.temp.day;
          }
        })
    }

    if(payload.cityName){
      result.cityName = payload.cityName;
    }

    if(!(Object.keys(result).length === 0)){
      return [result];
    }
    return payload.data.daily;
  }

  hourlyPipe(payload: Payload): any[]{
    let result: any = {}
    if(payload.data.hourly){
      payload.data.hourly
        .map((el: any, i: number) => {
          const date = formatDate(
            el.dt*1000,
            'HH',
            'en-US');

          if(+date % 3 === 0) {
            result['col_' + date] = el.temp;
          }
        })
    }

    if(payload.cityName){
      result.cityName = payload.cityName;
    }

    if(!(Object.keys(result).length === 0)){
      return [result];
    }

    return payload.data.hourly;
  }

  print(o: any){
    console.log(o)
  }

}
